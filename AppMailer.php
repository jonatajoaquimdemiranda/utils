<?php

namespace App\Utils\AppMailer;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class AppMailer
{
    public function sendMail($layout, $mail_from, $name_from, $reply_to, $mail_to, $name_to, $subject){
        $data['layout'] = $layout;
        $data['mail_from'] = $mail_from;
        $data['name_from'] = $name_from;
        $data['reply_to'] = $reply_to;
        $data['mail_to'] = $mail_to;
        $data['name_to'] = $name_to;
        $data['subject'] = $subject;
        try {
            Mail::send('emails.' . $data['layout'], ['data' => $data], function ($m) use ($data) {
                $m->from($data['mail_from'], $data['name_from']);
                $m->replyTo($data['reply_to']);
                $m->to($data['mail_to'], $data['name_to'])->subject($data['subject']);
            });
        } catch (\Exception $e) {
            $dados = "";
            foreach ($data as $key => $value) {
                $dados .= "<br>".$key."=>".$value."<br>";
            }
            if ($dados != "") {
                $dados = "<br>Dados:".$dados;
            }
            Log::error('Erro ao enviar email: ' . $e->getMessage().$dados);
        }
    }
}